<?php
/*
Plugin Name:  Press Item Post Type
Description:  Create press items for Wordpress.
Version:      1.0.0
Author:       Marlow Perceval
License:      MIT License
*/

add_action('init', 'create_press_item_post_type');
function create_press_item_post_type() {

	register_taxonomy('press_item_publisher', 'press_item', array(
		'hierarchical' => false,
		'labels' => array(
			'name' => _x( 'Publishers', 'taxonomy general name' ),
			'singular_name' => _x( 'Publisher', 'taxonomy singular name' ),
			'search_items' => __( 'Search Publishers' ),
			'popular_items' => __( 'Popular Publishers' ),
			'all_items' => __( 'All Publishers' ),
			'parent_item' => null,
			'parent_item_colon' => null,
			'edit_item' => __( 'Edit Publisher' ),
			'update_item' => __( 'Update Publisher' ),
			'add_new_item' => __( 'Add New Publisher' ),
			'new_item_name' => __( 'New Publisher Name' ),
			'separate_items_with_commas' => __( 'Separate publishers with commas' ),
			'add_or_remove_items' => __( 'Add or remove publishers' ),
			'choose_from_most_used' => __( 'Choose from the most used publishers' ),
			'not_found' => __( 'No publishers found.' ),
			'menu_name' => __( 'Publishers' ),
		),
		'show_admin_column' => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var' => 'publisher',
		'rewrite' => array('slug' => 'publisher')
	));

	register_post_type('press_item',
		array(
			'labels' => array(
				'name' => __('Press'),
				'singular_name' => __('Press Item'),
				'menu_name' => __('Press'),
				'all_items' => __('All Press'),
				'add_new_item' => __('Add New Press Item'),
				'edit_item' => __('Edit Press Item'),
				'new_item' => __('New Press Item'),
				'view_item' => __('View Press Item'),
				'search_items' => __('Search Press'),
				'not_found' => __('No press found'),
				'not_found_in_trash' => __('No press found in Trash'),
			),
			'public' => true,
			'exclude_from_search' => false,
			'taxonomies' => array(
				'press_item_publisher'
			),
			'has_archive' => true,
			'menu_position' => 20,
			'menu_icon'   => 'dashicons-testimonial',
			'supports' => array(
				'title',
				'excerpt',
				'thumbnail'
			),
			'rewrite' => array(
				'slug' => 'press'
			)
		)
	);
}